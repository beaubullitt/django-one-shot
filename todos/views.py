from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import CreateForm, ItemForm


# Create your views here.
def todo_list_list(request):
    todo_all = TodoList.objects.all()
    context = {
        "todo_all": todo_all,
    }

    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_item = TodoList.objects.get(id=id)
    context = {
        "todo_item": todo_item,
    }
    return render(request, "todos/detail.html", context)


# CREATE
# WHY DONT WE NEED TO PASS IN AN ID?
def todo_list_create(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            new_list = form.save()
            return redirect("todo_list_detail", id=new_list.id)
    else:
        form = CreateForm()
        context = {
            "form": form,
        }
        return render(request, "todos/create.html", context)


# EDIT/UPDATE
# EXPLANATIONS INCLUDED
def todo_list_update(request, id):
    # Grabbing the specific to do, with all data from that one Todo
    todo_instance = TodoList.objects.get(id=id)
    # if the API request is a post/ipload
    if request.method == "POST":
        # new form molded data created from createform.
        # data passed in are the request API and the instance of to
        # before the change
        update = CreateForm(request.POST, instance=todo_instance)
        # .is_valid is a built in integrity check
        if update.is_valid():
            # ??? what does form.save() give you?
            new_list = update.save()
            # redirect to the html
            return redirect("todo_list_detail", id=new_list.id)
    # Else case for GET or something else
    else:
        # molding the current instance data to create form
        update = CreateForm(instance=todo_instance)
    context = {
        # update_form is passing in form-molded data based on the instane
        "update_form": update,
        # If you want, you can pass in the raw instance data to use with {{}}
        "todo_instance": todo_instance,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        print("ITS POSTDDDDDDDDDDDDDDDDDD")
        form = ItemForm(request.POST)
        print("Form is: ", form)
        print(form.is_valid())
        if form.is_valid():
            print("ITS VALLLLLIIIDDDDDDDDD")
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        new_item_form = ItemForm()
        context = {
            "new_item_form": new_item_form,
        }
        return render(request, "todos/new_item.html", context)


def todo_item_update(request, id):
    todo_instance = TodoItem.objects.get(id=id)
    print("todo big ISSSSSS: ", todo_instance)
    if request.method == "POST":
        print("POOOOOOSSSTTEEEDDDD")
        update = ItemForm(request.POST, instance=todo_instance)
        print("UPDAAAAETTTTTTTEEE CREATTEDD")
        if update.is_valid():
            update.save()
            return redirect("todo_list_detail", id=todo_instance.list.id)
    else:
        update = ItemForm(instance=todo_instance)
    context = {
        "update_form": update,
        "todo_instance": todo_instance,
    }
    return render(request, "todos/item_update.html", context)

# , id=new_list.id
